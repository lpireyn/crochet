# Crochet

**Crochet** is a [Git](https://git-scm.com/) hooks manager written as a single [Bash](https://www.gnu.org/software/bash) script.

[![Apache License 2.0](https://img.shields.io/static/v1?label=License&message=Apache%20License%202.0&style=flat-square&color=informational&logo=apache)](https://www.apache.org/licenses/LICENSE-2.0)
[![Semantic Versioning 2.0.0](https://img.shields.io/static/v1?label=Semantic%20Versioning&message=2.0.0&style=flat-square&color=informational)](https://semver.org/spec/v2.0.0)
[![Keep a Changelog 1.0.0](https://img.shields.io/static/v1?label=Keep%20a%20Changelog&message=1.0.0&style=flat-square&color=informational)](https://keepachangelog.com/en/1.0.0)

[![Hosted on GitLab](https://img.shields.io/static/v1?label=Hosted%20on&message=GitLab&style=flat-square&color=informational&logo=gitlab)](https://gitlab.com/lpireyn/crochet)
[![Latest release](https://img.shields.io/gitlab/v/release/lpireyn/crochet?label=Latest%20release&message=GitLab&style=flat-square&color=informational&logo=gitlab)](https://gitlab.com/lpireyn/crochet/-/releases/)
[![Pipeline](https://img.shields.io/gitlab/pipeline/lpireyn/crochet/main?label=Pipeline&style=flat-square&logo=gitlab)](https://gitlab.com/lpireyn/crochet/-/commits/main)

## Installation

### Requirements

- [Bash](https://www.gnu.org/software/bash/) 4.0+
- [Git](https://git-scm.com/)

**Note:**
The version of Bash installed in MacOS is very old (3.x) and is incompatible with *Crochet*.
A more recent version should be installed with [Homebrew](https://brew.sh/).

### Installation from sources

Clone the *Crochet* repository:

``` shell
git clone https://gitlab.com/lpireyn/crochet.git
```

This creates a `crochet` directory which contains the `crochet` script.

For convenience, the `crochet` script can be copied to a directory in your `PATH` (typically `~/.local/bin`, `~/bin` or `/usr/local/bin`).

## Usage

```
Usage: crochet [OPTIONS]... <COMMAND> [ARGS]...

Commands:
  install      Install the Crochet hooks
  uninstall    Uninstall the Crochet hooks
  init [PATH]  Initialize the project hooks in the given path (default: .crochet)

Options:
  --version  Display the Crochet version and exit
  -h|--help  Display this help and exit

Git configuration:
  crochet.hooksPath  Path of the project hooks in the working tree (default: .crochet)
  crochet.verbose    Whether the Crochet hooks should print messages (bool, default: false)
```

## Changelog

See [CHANGELOG](CHANGELOG.md).

## Contributing

See [CONTRIBUTING](CONTRIBUTING.md).

## License

Crochet is licensed under the [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0) ([local copy](LICENSE)).
