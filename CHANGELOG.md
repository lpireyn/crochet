# Changelog

This changelog is based on [Keep a Changelog 1.0.0](https://keepachangelog.com/en/1.0.0).

This project adheres to [Semantic Versioning 2.0.0](https://semver.org/spec/v2.0.0).

Issue numbers refer to the [GitLab issues](https://gitlab.com/lpireyn/crochet/-/issues).

## [Unreleased]

### Added

- `crochet` script
- `--version` option
- `-h`|`--help` option
- `install` command
- `uninstall` command
- `init` command
